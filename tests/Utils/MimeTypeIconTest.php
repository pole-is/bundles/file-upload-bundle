<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Tests\Utils;

use Irstea\FileUploadBundle\Utils\MimeTypeIcon;
use PHPUnit\Framework\TestCase;

/**
 * Class MimeTypeIconTest.
 */
class MimeTypeIconTest extends TestCase
{
    /**
     * @covers       \Irstea\FileUploadBundle\Utils\MimeTypeIcon::getMimeTypeIcon
     * @dataProvider getTestValues
     *
     * @param mixed $expected
     * @param mixed $input
     */
    public function testGetMimeTypeIcon($expected, $input)
    {
        static::assertEquals($expected, MimeTypeIcon::getMimeTypeIcon($input));
    }

    /**
     * @return array
     */
    public function getTestValues()
    {
        return [
            [null, ''],
            [null, []],
            ['audio', 'audio/mpeg'],
            ['text', 'text/plain'],
            ['video', 'video/mpeg'],
            ['image', 'image/png'],
            ['pdf', 'application/pdf'],
            ['archive', 'application/zip'],
            ['text', 'text/plain; charset=UTF-8'],
            ['archive', 'application/zip; format=lzh'],
        ];
    }
}
