<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Irstea\FileUploadBundle\Entity\UploadedFile;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * UploadedFile controller.
 *
 * @Route("/files")
 * @Security("is_granted('ROLE_FILES_ADMIN')")
 */
class UploadedFileController extends Controller
{
    /**
     * Lists all UploadedFile entities.
     *
     * @Route("/", name="files")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var EntityRepository $repo */
        $repo = $em->getRepository('IrsteaFileUploadBundle:UploadedFile');

        $queryBuilder = $repo
            ->createQueryBuilder('u')
            ->orderBy('u.createdAt', 'DESC');

        $pager = new Pagerfanta(new DoctrineORMAdapter($queryBuilder));
        $pager->setCurrentPage($request->query->get('page', 1));

        $uploadedFiles = $pager->getCurrentPageResults();

        return [
            'uploadedFiles' => $uploadedFiles,
            'pager'         => $pager,
        ];
    }

    /**
     * Finds and displays a UploadedFile entity.
     *
     * @Route("/{id}", name="files_show")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function showAction(UploadedFile $uploadedFile)
    {
        return [
            'uploadedFile' => $uploadedFile,
        ];
    }

    /**
     * @param string $message
     */
    public function notice($message, array $parameters = [])
    {
        /** @var LoggerInterface $logger */
        $logger = $this->get('monolog.logger.irstea_logger') ?? new NullLogger();
        $logger->notice($message, $parameters);
    }
}
