<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Controller;

use Irstea\FileUploadBundle\Entity\UploadedFile;
use Irstea\FileUploadBundle\Exception\RejectedFileException;
use Irstea\FileUploadBundle\Http\UploadedFileResponse;
use Irstea\FileUploadBundle\Model\FileManagerInterface;
use Irstea\FileUploadBundle\Model\UploadedFileInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * @Route("/api/files", service="irstea_file_upload.upload_controller")
 */
class UploadController extends Controller
{
    public const CSRF_INTENTION = 'uploaded_file';

    protected const REQUEST_FORMAT = 'json';

    /**
     * @var FileManagerInterface
     */
    protected $fileManager;

    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    /**
     * @var CsrfTokenManagerInterface
     */
    protected $csrfTokenManager;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * UploadController constructor.
     */
    public function __construct(
        FileManagerInterface $fileManager,
        UrlGeneratorInterface $urlGenerator,
        CsrfTokenManagerInterface $csrfTokenManager,
        TokenStorageInterface $tokenStorage,
        EngineInterface $templating
    ) {
        $this->fileManager = $fileManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->tokenStorage = $tokenStorage;
        $this->templating = $templating;
    }

    /**
     * @Route("", name="file_upload_create")
     * @Method("POST")
     */
    public function createAction(Request $request): JsonResponse
    {
        $request->setRequestFormat(self::REQUEST_FORMAT);
        $data = $request->request->get('file');

        $token = $this->tokenStorage->getToken();

        $file = $this->fileManager->create(
            $data['name'],
            (int) $data['size'],
            $data['type'],
            (int) $data['lastModified'] ?: null
        );

        $parameters = ['id' => $file->getId()];

        $deleteUrl = $this->urlGenerator->generate('file_upload_delete', $parameters);

        return $this->createResponse(
            Response::HTTP_CREATED,
            'New file created',
            array_merge(
                $file->toArray(),
                [
                    'put_url'     => $this->urlGenerator->generate('file_upload_put_content', $parameters),
                    'delete_type' => 'DELETE',
                    'delete_url'  => $deleteUrl,
                ]
            ),
            // On a pas de get pour l'instant, le DELETE et ce qui y ressemble le plus
            ['Location' => $deleteUrl]
        );
    }

    /**
     * @Route("/{id}/content", name="file_upload_put_content")
     * @Method("PUT")
     *
     * @return JsonResponse|Response
     */
    public function putContentAction(Request $request, UploadedFile $file): Response
    {
        $request->setRequestFormat(self::REQUEST_FORMAT);
        $this->validateCsrfToken($request);

        [$offset, $maxlen, $complete] = $this->handleRangeHeader($request);

        // Demande un filehandle plutôt que charger le contenu en mémoire
        $input = $request->getContent(true);
        $file->copyFrom($input, $maxlen, $offset);
        fclose($input);

        if ($complete) {
            return $this->completeUpload($file);
        }

        return $this->createResponse(Response::HTTP_OK, 'Chunk received');
    }

    protected function handleRangeHeader(Request $request): array
    {
        if (!$request->headers->has('Content-Range')) {
            return [0, PHP_INT_MAX, true];
        }
        $range = $request->headers->get('Content-Range');

        $matches = [];
        if (!preg_match('@^bytes (\d+)-(\d+)/(\d+)$@', $range, $matches)) {
            throw new BadRequestHttpException('Invalid Content-Range');
        }

        $start = (int) $matches[1];
        $end = (int) $matches[2];
        $total = (int) $matches[3];

        if ($start < 0 || $start >= $end || $end >= $total) {
            throw new HttpException(Response::HTTP_REQUESTED_RANGE_NOT_SATISFIABLE);
        }

        return [$start, 1 + ($end - $start), $end === ($total - 1)];
    }

    protected function completeUpload(UploadedFileInterface $file): Response
    {
        try {
            $this->fileManager->completed($file);
        } catch (RejectedFileException $ex) {
            return $this->createResponse(Response::HTTP_FORBIDDEN, 'File rejected: ' . $ex->getMessage());
        }

        $parameters = ['id' => $file->getId()];
        $fileData = array_merge(
            $file->toArray(),
            [
                'url'         => $this->urlGenerator->generate('file_upload_get_content', $parameters),
                'delete_type' => 'DELETE',
                'delete_url'  => $this->urlGenerator->generate('file_upload_delete', $parameters),
                'repr'        => $this->templating->render(
                    'IrsteaFileUploadBundle:Extension:uploaded_file.html.twig',
                    ['file' => $file->toArray()]
                ),
            ]
        );

        return $this->createResponse(Response::HTTP_OK, 'File uploaded.', ['files' => [$fileData]]);
    }

    /**
     * @Route("/{id}/content", name="file_upload_get_content")
     * @Method("GET")
     */
    public function getContentAction(Request $request, UploadedFile $file): UploadedFileResponse
    {
        $this->validateCsrfToken($request);

        if (!$file->isValid()) {
            throw new NotFoundHttpException();
        }

        $response = UploadedFileResponse::create($file, 200, [], false, ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $response->isNotModified($request);

        return $response;
    }

    /**
     * @Route("/{id}", name="file_upload_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, UploadedFile $file): JsonResponse
    {
        $request->setRequestFormat(self::REQUEST_FORMAT);

        $this->validateCsrfToken($request);

        $this->fileManager->delete($file);

        return $this->createResponse();
    }

    /**
     * @throws HttpException
     */
    protected function validateCsrfToken(Request $request): void
    {
        $token = $this->csrfTokenManager->getToken($request->query->get('token'));
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'Invalid CSRF token');
        }
    }

    /**
     * @param int    $status
     * @param string $message
     */
    protected function createResponse($status = Response::HTTP_OK, $message = 'OK', array $data = [], array $headers = []): JsonResponse
    {
        $data['status'] = $status;
        $data['message'] = $message;
        $response = new JsonResponse($data, $status, $headers);
        $response->setStatusCode($status, $message);

        return $response;
    }
}
