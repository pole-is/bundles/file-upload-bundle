<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Validation;

use Irstea\FileUploadBundle\Model\UploadedFileInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Vérifie que le type MIME du fichier valide la contrainte.
 */
class FileMimeTypeValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!($value instanceof UploadedFileInterface) || !($constraint instanceof FileMimeType)) {
            return;
        }
        if (preg_match('@^' . $constraint->acceptedMimeTypes . '$@i', $value->getMimeType())) {
            return;
        }
        $this->context->buildViolation($constraint->message)
            ->setParameter('%displayName%', $value->getDisplayName())
            ->setParameter('%mimeType%', $value->getMimeType())
            ->addViolation();
    }
}
