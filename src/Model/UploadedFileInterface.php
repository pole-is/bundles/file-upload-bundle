<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Model;

use DateTime;
use Gaufrette\Filesystem;

/** Un fichier uploadé.
 */
interface UploadedFileInterface
{
    /** Fichier en cours d'upload.
     * @var string
     */
    public const ETAT_EN_COURS = 'en-cours';

    /** Fichier orphelin.
     * Fichier qui n'a pas encore été associé à un objet métier.
     *
     * @var string
     */
    public const ETAT_ORPHELIN = 'orphelin';

    /** Fichier normal.
     * Fichier associé à un objet métier.
     *
     * @var string
     */
    public const ETAT_NORMAL = 'normal';

    /** Fichier corrompu.
     * Fichier dont le contenu disque ne correspond pas à la somme de contrôle enregistreé en base de données.
     *
     * @var string
     */
    public const ETAT_CORROMPU = 'corrompu';

    /** Fichier manquant.
     * Fichier existant en base de données mais introuvable sur disque.
     *
     * @var string
     */
    public const ETAT_MANQUANT = 'manquant';

    /** Fichier rejeté.
     * Fichier uploadé mais rejeté par un listener (par exemple l'antivirus).
     *
     * @var string
     */
    public const ETAT_REJETE = 'rejete';

    /** Retourne l'identifiant du fichier.
     * @return string
     *
     * @api
     */
    public function getId();

    /**
     * Set originalFilename.
     *
     * @param string $displayName
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setDisplayName($displayName);

    /** Retourne le nom original du fichier (c-à-d sur le poste de l'utilisateur ayant fait l'upload.)
     * @return string
     *
     * @api
     */
    public function getDisplayName();

    /**
     * @param string $description
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setDescription($description = null);

    /** Retourne la description du fichier.
     * @return string
     *
     * @api
     */
    public function getDescription();

    /** Retourne le chemin du fichier dans le filesystem.
     * @return string
     *
     * @api
     */
    public function getPath();

    /** Remplace le chemin du fichier dans le filesystem.
     * Cela devrait causer un déplacement/renommage du fichier.
     *
     * @param string $path
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setPath($path);

    /** Change le chemin du fichier sans changer le nom.
     * @param string $newDir Nouveau répertoire
     *
     * @return UploadedFileInterface
     *
     * @uses \UploadedFileInterface::setPath
     *
     * @api
     */
    public function moveTo($newDir);

    /**
     * Set mimeType.
     *
     * @param string $mimeType
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setMimeType($mimeType);

    /** Retourne le type MIME enregistré.
     * @return string
     *
     * @api
     */
    public function getMimeType();

    /**
     * Set size.
     *
     * @param int $size
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setSize($size);

    /** Retourne la taille enregistrée, en octets.
     * @return int
     *
     * @api
     */
    public function getSize();

    /**
     * Set checksum.
     *
     * @param string $checksum
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setChecksum($checksum);

    /** Retourne la somme de contrôle enregistrée.
     * @return string
     *
     * @api
     */
    public function getChecksum();

    /** Modifie l'état courant du fichier.
     * @param string $etat
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setEtat($etat);

    /** Retourne l'état courant du fichier.
     * @return string
     *
     * @api
     */
    public function getEtat();

    /** Définit la date de création du fichier.
     *
     * @return self
     *
     * @api
     */
    public function setCreatedAt(\DateTime $time);

    /** Définit le nom de l'utilisateur ayant uploadé le fichier.
     * @param string $username
     *
     * @return self
     *
     * @api
     */
    public function setCreatedBy($username);

    /** Retourne l'adresse IP du client ayant uploadé le fichier.
     * @param string $ipAddress
     *
     * @return self
     *
     * @api
     */
    public function setCreatedFrom($ipAddress);

    /** Retourne la date de création du fichier.
     * @return DateTime
     *
     * @api
     */
    public function getCreatedAt();

    /** Retourne le nom de l'utilisateur ayant uploadé le fichier.
     * @return string
     *
     * @api
     */
    public function getCreatedBy();

    /** Retourne l'adresse IP du client ayant uploadé le fichier.
     * @return string
     *
     * @api
     */
    public function getCreatedFrom();

    /** Remplace les métadonnées du fichier.
     * @param array $metadata les nouvelles métadonnées
     *
     * @return UploadedFileInterface
     *
     * @api
     */
    public function setMetadata(array $metadata);

    /** Retourne les métadonnées du fichier.
     * @return array
     *
     * @api
     */
    public function getMetadata();

    /** Retourne une représentation texte du fichier.
     * @return string
     *
     * @api
     */
    public function __toString();

    /** Vérifie la validité du fichier.
     * Cela consiste à vérifier que le fichier existe dans le filesystem, et que la taille et la somme de contrôle
     * correspondent. En cas d'erreur, l'état du fichier est modifié en conséquence.
     *
     * @uses \UploadedFileInterface::setEtat
     *
     * @api
     */
    public function validate();

    /** Détermine si le fichier est dans un état "valide".
     * @return bool
     *
     * @api
     */
    public function isValid();

    /** Détermine si le fichier est orphelin.
     * @return bool
     *
     * @api
     */
    public function isOrphelin();

    /** Retourne la date de dernière modification dans le filesystem.
     * @return DateTime|null
     *
     * @api
     */
    public function getLastModified();

    /** Retourne le contenu du fichier.
     * @return string une chaîne
     *
     * @api
     */
    public function getContent();

    /** Remplace le contenu du fichier.
     * @param string $content
     *
     * @api
     */
    public function setContent($content);

    /** Ecrit dans le fichier depuis un descripteur de fichier.
     * @param resource $source      flux d'entrée à lire
     * @param int      $maxlen      nombre maximum d'octets à lire
     * @param int      $writeOffset offset depuis le début du fichier
     *
     * @return int nombre d'octets écrits
     *
     * @api
     */
    public function copyFrom($source, $maxlen = -1, $writeOffset = 0);

    /** Envoie le contenu du fichier dans un descripteur de fichier.
     * @param resource $dest       flux de sortie
     * @param int      $maxlen     nombre maximum d'octets à lire
     * @param int      $readOffset offset depuis le début du fichier
     *
     * @return int nombre d'octets lus
     *
     * @api
     */
    public function copyTo($dest, $maxlen = PHP_INT_MAX, $readOffset = 0);

    /** Retourne un nom de fichier local pour ce fichier.
     * Ce peut-être un fichier temporaire qui sera supprimé à la fin de la requête.
     *
     * @return string
     *
     * @api
     */
    public function getLocalPath();

    /** Retourne une représentation "tableau" des attributs du fichier uploadé.
     * Utilisé comme sérialisation du pauvre.
     *
     * @todo Utiliser un vrai serializer.
     *
     * @return array
     *
     * @internal
     */
    public function toArray();

    /**
     * Détermine si ce fichier a le même contenu qu'un autre fichier.
     *
     * @return bool true si les deux fichiers ont le même contenu
     *
     * @api
     */
    public function hasSameContent(UploadedFileInterface $other);

    public function setFilesystem(Filesystem $fs): void;
}
