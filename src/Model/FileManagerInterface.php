<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Model;

use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * Gestionnaire de fichier uploadé.
 * Cette interface est le point d'entrée obligatoire pour toute manipulation de fichiers uploadés.
 */
interface FileManagerInterface
{
    /** Retrouve un fichier uploadé par son identifiant.
     * @param string $id identifiant du fichier
     *
     * @return UploadedFileInterface|null le fichier retrouvé ou null si aucun fichier ne correspond
     *
     * @api
     */
    public function get($id): ?UploadedFileInterface;

    /** Crée un nouveau fichier uploadé, vide.
     * Le fichier est créé à l'état UploadedFileInterface::ETAT_EN_COURS.
     *
     * @param string $name         nom original du fichier
     * @param int    $size         taille prévisionnelle, en octets
     * @param string $mimeType     mime TYPE provisoire
     * @param int    $lastModified timestamp de la date de dernière modification
     *
     * @return UploadedFileInterface le fichier créé
     *
     * @api
     */
    public function create(string $name, int $size, string $mimeType, int $lastModified = null): UploadedFileInterface;

    /** Duplique un fichier.
     * N'accepte que des fichiers valides.
     * Le nouveau fichier est automatiquement orphelin, avec un chemin par défaut.
     *
     * @throws InvalidArgumentException le fichier original est invalide
     *
     * @api
     */
    public function duplicate(UploadedFileInterface $original): UploadedFileInterface;

    /** Supprime un fichier uploadé.
     * *Attention :* le fichier est supprimé du disque !
     *
     * @param UploadedFileInterface $file fichier à supprimer
     *
     * @api
     */
    public function delete(UploadedFileInterface $file): void;

    /** Indique que l'upload d'un fichier est terminé.
     * Cela passe le fichier à l'état UploadedFileInterface::ORPHELIN et provoque la mise-à-jour de la taille, du
     * type MIME et de la somme de contrôle.
     *
     * @param UploadedFileInterface $file fichier terminé
     *
     * @internal
     */
    public function completed(UploadedFileInterface $file): void;

    /** Retourne une liste de fichiers invalides à supprimer.
     * Ce sont des fichiers partiels ou orphelin qui n'ont pas été modifiés depuis plus d'une heure.
     *
     * @return UploadedFileInterface[]
     *
     * @internal
     */
    public function findGarbage(): array;

    /** Retourne la liste des fichiers à valider.
     * @return UploadedFileInterface[]
     *
     * @internal
     */
    public function findFilesToValidate(): array;
}
