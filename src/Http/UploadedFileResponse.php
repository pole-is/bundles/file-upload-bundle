<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Http;

use Irstea\FileUploadBundle\Model\UploadedFileInterface;
use LogicException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Description of UploadedFileResponse.
 * Très, très, fortement inspiré de Symfony\Component\HttpFoundation\UploadedFileResponse.
 */
class UploadedFileResponse extends Response
{
    /**
     * @var UploadedFileInterface
     */
    protected $file;

    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $maxlen;

    /**
     * Constructor.
     *
     * @param UploadedFileInterface $file               The file to stream
     * @param int                   $status             The response status code
     * @param array                 $headers            An array of response headers
     * @param bool                  $public             Files are public by default
     * @param string|null           $contentDisposition The type of Content-Disposition to set automatically with the filename
     */
    public function __construct(
        UploadedFileInterface $file = null,
        $status = 200,
        $headers = [],
        $public = true,
        $contentDisposition = null
    ) {
        parent::__construct(null, $status, $headers);

        if ($file) {
            $this->setFile($file, $contentDisposition);
        }

        if ($public) {
            $this->setPublic();
        }
    }

    /**
     * @param UploadedFileInterface $file               The file to stream
     * @param int                   $status             The response status code
     * @param array                 $headers            An array of response headers
     * @param bool                  $public             Files are public by default
     * @param string|null           $contentDisposition The type of Content-Disposition to set automatically with the filename
     *
     * @return UploadedFileResponse The created response
     */
    public static function create($file = null, $status = 200, $headers = [], $public = true, $contentDisposition = null)
    {
        return new static($file, $status, $headers, $public, $contentDisposition);
    }

    /**
     * Sets the file to stream.
     *
     * @param UploadedFileInterface $file               The file to stream
     * @param string                $contentDisposition
     *
     * @throws FileException
     *
     * @return UploadedFileResponse
     */
    public function setFile(UploadedFileInterface $file, $contentDisposition = null)
    {
        $this->file = $file;

        $this
            ->setLastModified($this->file->getLastModified())
            ->setEtag($file->getChecksum());

        if ($contentDisposition) {
            $this->setContentDisposition($contentDisposition);
        }

        return $this;
    }

    /**
     * Gets the file.
     *
     * @return UploadedFileInterface The file to stream
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets the Content-Disposition header with the given filename.
     *
     * @param string $disposition      ResponseHeaderBag::DISPOSITION_INLINE or ResponseHeaderBag::DISPOSITION_ATTACHMENT
     * @param string $filename         Optionally use this filename instead of the real name of the file
     * @param string $filenameFallback A fallback filename, containing only ASCII characters. Defaults to an automatically encoded filename
     *
     * @return UploadedFileResponse
     */
    public function setContentDisposition($disposition, $filename = '', $filenameFallback = '')
    {
        if ($filename === '') {
            $filename = $this->file->getDisplayName();
        }
        if ($filenameFallback === '') {
            $filenameFallback = preg_replace('/[^\x20-\x7e]/', '_', $filename);
        }

        $dispositionHeader = $this->headers->makeDisposition($disposition, $filename, $filenameFallback);
        $this->headers->set('Content-Disposition', $dispositionHeader);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function prepare(Request $request)
    {
        $this->headers->set('Content-Length', (string) $this->file->getSize());

        if (!$this->headers->has('Accept-Ranges')) {
            // Only accept ranges on safe HTTP methods
            $this->headers->set('Accept-Ranges', $request->isMethodSafe() ? 'bytes' : 'none');
        }

        if (!$this->headers->has('Content-Type')) {
            $this->headers->set('Content-Type', $this->file->getMimeType() ?: 'application/octet-stream');
        }

        if ($request->server->get('SERVER_PROTOCOL') !== 'HTTP/1.0') {
            $this->setProtocolVersion('1.1');
        }

        $this->ensureIEOverSSLCompatibility($request);

        $this->offset = 0;
        $this->maxlen = -1;

        if ($request->headers->has('Range')) {
            // Process the range headers.
            if (!$request->headers->has('If-Range') || $this->getEtag() === $request->headers->get('If-Range')) {
                $range = $request->headers->get('Range');
                $fileSize = $this->file->getSize();

                [$start, $end] = explode('-', substr($range, 6), 2) + [0];

                $end = ($end === '') ? $fileSize - 1 : (int) $end;

                if ($start === '') {
                    $start = $fileSize - $end;
                    $end = $fileSize - 1;
                } else {
                    $start = (int) $start;
                }

                if ($start <= $end) {
                    if ($start < 0 || $end > $fileSize - 1) {
                        $this->setStatusCode(416);
                    } elseif ($start !== 0 || $end !== $fileSize - 1) {
                        $this->maxlen = $end < $fileSize ? $end - $start + 1 : -1;
                        $this->offset = $start;

                        $this->setStatusCode(206);
                        $this->headers->set('Content-Range', sprintf('bytes %s-%s/%s', $start, $end, $fileSize));
                        $this->headers->set('Content-Length', (string) ($end - $start + 1));
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Sends the file.
     */
    public function sendContent()
    {
        if (!$this->isSuccessful()) {
            parent::sendContent();

            return;
        }

        if (0 === $this->maxlen) {
            return;
        }

        $out = fopen('php://output', 'wb');
        $this->file->copyTo($out, $this->maxlen, $this->offset);
        fclose($out);
    }

    /**
     * {@inheritdoc}
     *
     * @throws LogicException when the content is not null
     */
    public function setContent($content)
    {
        if (null !== $content) {
            throw new LogicException('The content cannot be set on a UploadedFileResponse instance.');
        }
    }

    /**
     * {@inheritdoc}
     *
     * @return false
     */
    public function getContent()
    {
        return '';
    }
}
