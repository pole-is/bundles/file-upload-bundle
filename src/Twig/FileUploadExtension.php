<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Twig;

use InvalidArgumentException;
use Irstea\FileUploadBundle\Entity\UploadedFile;
use Irstea\FileUploadBundle\Model\UploadedFileInterface;
use Irstea\FileUploadBundle\Service\FileUrlGeneratorInterface;
use Irstea\FileUploadBundle\Utils\MimeTypeIcon;
use NumberFormatter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

/** Extension Twig pour les fichiers uploadés.
 */
class FileUploadExtension extends Twig_Extension
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var FileUrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(TranslatorInterface $translator, FileUrlGeneratorInterface $urlGenerator)
    {
        $this->translator = $translator;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new Twig_SimpleFunction(
                'irstea_uploaded_file',
                [$this, 'formatUploadedFile'],
                [
                    'needs_environment' => true,
                    'is_safe'           => ['html'],
                ]
            ),
            new Twig_SimpleFunction(
                'irstea_file_url',
                [$this, 'generateFileUrl'],
                [
                    'is_safe' => ['html', 'html_attr'],
                ]
            ),
            new Twig_SimpleFunction(
                'irstea_file_icon',
                [$this, 'formatFileIcon'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter(
                'irstea_file_size',
                [$this, 'formatFileSize'],
                ['needs_context' => true]
            ),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'irstea_file_upload';
    }

    /** Genère un tag HTML pour représenter le fichier.
     * Inclut un lien sécurisé qui permet de télécharger le fichier, mais uniquement à l'utilisateur qui a affiché la page.
     * Usage : `{{ irstea_uploaded_file(file) }}`.
     *
     * @param UploadedFileInterface $file
     *
     * @return string
     */
    public function formatUploadedFile(Twig_Environment $env, $file = null)
    {
        if (null === $file) {
            return '';
        } elseif ($file instanceof UploadedFile) {
            $file = $file->toArray();
        } elseif (!is_array($file)) {
            throw new InvalidArgumentException('irstea_uploaded_file expects an UploadedFile instance, array or null');
        }

        return $env->render('IrsteaFileUploadBundle:Extension:uploaded_file.html.twig', ['file' => $file]);
    }

    /** Genère un URL sécurisée pour télécharger un fichier.
     * Usage : `{{ irstea_file_path(file.id) }}`.
     *
     * @param string          $idFile
     * @param bool|int|string $referenceType
     *
     * @return string
     */
    public function generateFileUrl($idFile, $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->urlGenerator->generate($idFile, $referenceType);
    }

    /** Génère un tag "icône" Font-Awesome pour le type MIME indiqué.
     * Usage : `{{ irstea_file_icon(mimeType) }}`.
     *
     * @param string $mimeType type MIME
     *
     * @return string un tag <i></i> avec les classes Font-Awesome
     *
     * @uses \MimeTypeIcon::getMimeTypeIcon
     */
    public function formatFileIcon($mimeType)
    {
        $icon = MimeTypeIcon::getMimeTypeIcon($mimeType);

        return sprintf('<i class="icon fa fa-file%s-o"></i>', ($icon && $icon !== 'file') ? ('-' . $icon) : '');
    }

    /** Formate une taille de fichier.
     * Usage : `{{ size|irstea_file_size([precision][, locale]) }}`.
     *
     * @param array  $context   contexte Twig
     * @param int    $size      taille du fichier
     * @param int    $precision nombre de chiffres après la virgule
     * @param string $locale    Langue
     *
     * @return string
     *
     * @uses \NumberFormatter
     */
    public function formatFileSize($context, $size, $precision = null, $locale = null)
    {
        if (!is_numeric($size)) {
            return '';
        }

        if ($size > 1000000000) {
            $size /= 1000000000.0;
            $defaultPrecision = 2;
            $unit = 'Gi';
        } elseif ($size > 1000000) {
            $size /= 1000000.0;
            $defaultPrecision = 1;
            $unit = 'Mi';
        } elseif ($size > 1000) {
            $size /= 1000.0;
            $defaultPrecision = 1;
            $unit = 'Ki';
        } else {
            $unit = '';
            $defaultPrecision = 0;
        }

        if (null === $locale) {
            $locale = $context['app']->getRequest()->getLocale();
        }

        $formatter = NumberFormatter::create($locale, NumberFormatter::DECIMAL);
        $formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, $precision ?? 0);
        $formatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, $precision ?? $defaultPrecision);

        return $this->translator->trans(
            'file_size(%size%,%unit%)',
            [
                '%size%' => $formatter->format($size),
                '%unit%' => $unit,
            ],
            'file_upload',
            $locale
        );
    }
}
