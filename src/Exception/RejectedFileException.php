<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Exception;

use Irstea\FileUploadBundle\Model\UploadedFileInterface;
use RuntimeException;

/**
 * Description of RejectedFileException.
 */
class RejectedFileException extends RuntimeException implements Exception
{
    /**
     * @var UploadedFileInterface
     */
    protected $uploadedFile;

    /**
     * @param string     $message
     * @param int        $code
     * @param \Exception $previous
     *
     * @internal param UploadedFileInterface $file
     */
    public function __construct(UploadedFileInterface $uploadedFile, $message, $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->uploadedFile = $uploadedFile;
    }

    /**
     * @return UploadedFileInterface
     */
    public function getUploadedFile()
    {
        return $this->uploadedFile;
    }
}
