<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Form\Type;

use Irstea\FileUploadBundle\Form\DataTranformer\UploadedFileTransformer;
use Irstea\FileUploadBundle\Model\FileManagerInterface;
use Irstea\FileUploadBundle\Validation\FileMimeType;
use Irstea\FileUploadBundle\Validation\FileSize;
use Irstea\ThemeBundle\Form\AbstractJQueryWidgetTrait;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\EventListener\MergeCollectionListener;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Router;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\NotBlank;

/** Type de champ 'file_upload'.
 * Options :.
 * * multiple (boolean) : fichier unique ou multiple ? false par défaut.
 * * accept_file_types (string) : expression régulière de contrôle des types MIME acceptés. Aucune par défaut.
 * * max_file_size (integer) : taille maximale d'un fichier. 0 (illimité) par défaut.
 * * min_file_size (integer) : taille minimale d'un fichier. 0 par défaut.
 * * max_number_of_files (integer) : nombre maximum de fichiers si multiple vaut true. 0 (illimité par défaut).
 * * bitrate_interval (integer) : période de mise à jour du débit d'upload, en ms. 512ms par défaut.
 * * progress_interval (integer): période de mis à jour de la progression, en ms. 100ms par défaut.
 * * recalculate_progress (boolean) : recalculer la progression en cas d'erreur ? true par défaut.
 * * sequential_uploads (boolean) : force un upload séquentiel des fichiers ? false par défaut.
 * * max_chunk_size (integer) : taille maximule des parties envoyées, en octets. 0 (envoyer le fichier en une seule requête) par défaut.
 */
class FileUploadType extends AbstractType
{
    use AbstractJQueryWidgetTrait;

    /** Routeur.
     * @var Router
     */
    private $router;

    /** Gestionnaire de fichiers.
     * @var FileManagerInterface
     */
    private $fileManager;

    /** Taille par défaut pour l'envoi par morceaux.
     * @var int
     */
    private $defaultMaxChunkSize;

    /**
     * @param int $defaultMaxChunkSize
     */
    public function __construct(Router $router, FileManagerInterface $fileManager, $defaultMaxChunkSize)
    {
        $this->router = $router;
        $this->fileManager = $fileManager;
        $this->defaultMaxChunkSize = $defaultMaxChunkSize;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setCompound(false);

        if ($options['multiple']) {
            $builder->addEventSubscriber(new MergeCollectionListener(true, true));
            $builder->addViewTransformer(new CollectionToArrayTransformer());
        }

        $builder->addViewTransformer(new UploadedFileTransformer($this->fileManager, $options['multiple']));
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Désactive la vérification du type MIME côté client, car elle n'est pas assez fiable.
        unset($options['accept_file_types']);

        $this->buildWidgetView($view, $form, $options);

        $view->vars['widget_attr']['data-create-url'] = $this->router->generate('file_upload_create');
        $view->vars['widget_attr']['data-required'] = $options['required'];
        $view->vars['multiple'] = $options['multiple'];
        if ($options['max_chunk_size'] > 0) {
            $view->vars['widget_attr']['data-max-chunk-size'] = $options['max_chunk_size'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $this->setWidgetDefaultOptions($resolver);

        $resolver->setDefaults(
            [
                'max_chunk_size' => $this->defaultMaxChunkSize ?: 0,
                'error_bubbling' => false,
            ]
        );

        $resolver->addAllowedTypes('max_chunk_size', 'int');

        $resolver->setNormalizer(
            'constraints',
            function (OptionsResolver $options, $constraints) {
                return $this->addConstraints($options, $constraints);
            }
        );
    }

    /** Ajoute des contraintes de validation correspondants aux paramètres du champ.
     * @todo Implémenter min_file_size, max_file_size && accept_file_types.
     *
     * @param mixed $constraints
     *
     * @return array|mixed
     */
    protected function addConstraints(OptionsResolver $options, $constraints)
    {
        if ($options['multiple']) {
            $range = [];
            if ($options['required']) {
                $range['min'] = 1;
                $range['minMessage'] = 'file_upload.required';
            }
            if ($options['max_number_of_files']) {
                $range['max'] = $options['max_number_of_files'];
                $range['maxMessage'] = 'file_upload.maxNumberOfFiles';
            }
            if (!empty($range)) {
                $constraints[] = new Count($range);
            }
        } elseif ($options['required']) {
            $required = new NotBlank(['message' => 'file_upload.required']);
            $constraints[] = $required;
        }

        if ($options['min_file_size'] || $options['max_file_size']) {
            $constraints[] = new FileSize(
                [
                    'min' => $options['min_file_size'],
                    'max' => $options['max_file_size'],
                ]
            );
        }

        if ($options['accept_file_types']) {
            $constraints[] = new FileMimeType($options['accept_file_types']);
        }

        return $constraints;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'file_upload';
    }

    /**
     * {@inheritdoc}des morceaux
     */
    protected function getWidgetDefaults()
    {
        return [
            'multiple'             => false,
            'accept_file_types'    => '',
            'max_file_size'        => 0,
            'min_file_size'        => 0,
            'max_number_of_files'  => 0,
            'bitrate_interval'     => 512,
            'progress_interval'    => 100,
            'recalculate_progress' => true,
            'sequential_uploads'   => false,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getWidgetType()
    {
        return 'irsteaFileUpload';
    }
}
