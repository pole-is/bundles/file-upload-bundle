<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Form\DataTranformer;

use Irstea\FileUploadBundle\Model\FileManagerInterface;
use Irstea\FileUploadBundle\Model\UploadedFileInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Traversable;

/**
 * Description of UploadedFileTransformer.
 */
class UploadedFileTransformer implements DataTransformerInterface
{
    /**
     * @var bool
     */
    private $multiple;

    /**
     * @var FileManagerInterface
     */
    private $fileManager;

    /**
     * UploadedFileTransformer constructor.
     *
     * @param bool $multiple
     */
    public function __construct(FileManagerInterface $fileManager, $multiple = false)
    {
        $this->fileManager = $fileManager;
        $this->multiple = $multiple;
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($value)
    {
        if (!$this->multiple) {
            return $this->reverseTransformFile($value);
        }

        if (empty($value)) {
            return [];
        }

        if (!is_array($value) && !$value instanceof Traversable) {
            throw new TransformationFailedException('Expected an array or a \Traversable.');
        }

        $result = [];
        foreach ($value as $identifier) {
            $file = $this->reverseTransformFile($identifier);
            if ($file !== null) {
                $result[] = $file;
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($value)
    {
        if (!$this->multiple) {
            return $this->transformFile($value);
        }

        if (empty($value)) {
            return [];
        }

        if (!is_array($value) && !$value instanceof Traversable) {
            throw new TransformationFailedException('Expected an array or a \Traversable.');
        }

        $result = [];
        foreach ($value as $file) {
            $item = $this->transformFile($file);
            if ($item !== null) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    public function transformFile($value)
    {
        if (empty($value)) {
            return null;
        }

        if (!$value instanceof UploadedFileInterface) {
            throw new TransformationFailedException('Expected an UploadedFileInterface.');
        }

        return $value->toArray();
    }

    /**
     * @param mixed $value
     */
    public function reverseTransformFile($value): ?UploadedFileInterface
    {
        if (empty($value)) {
            return null;
        }

        if (!is_array($value)) {
            throw new TransformationFailedException(sprintf('Expected an array, not a %s.', is_object($value) ? get_class($value) : gettype($value)));
        }

        if (empty($value['id'])) {
            return null;
        }

        $identifier = $value['id'];

        $file = $this->fileManager->get($identifier);
        if (null === $file) {
            return null;
        }

        if ($file->isOrphelin()) {
            $file->setEtat(UploadedFileInterface::ETAT_NORMAL);
        }
        $file->setDescription(!empty($value['description']) ? $value['description'] : null);

        return $file;
    }
}
