<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Service;

use Irstea\FileUploadBundle\Controller\UploadController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * Implémentation de FileUrlInterface utilisant les services de Symfony.
 */
class FileUrlGenerator implements FileUrlGeneratorInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    protected $urlGenerator;

    /**
     * @var CsrfTokenManagerInterface
     */
    protected $tokenManager;

    public function __construct(UrlGeneratorInterface $urlGenerator, CsrfTokenManagerInterface $tokenManager)
    {
        $this->urlGenerator = $urlGenerator;
        $this->tokenManager = $tokenManager;
    }

    /**
     * {@inheritdoc}
     */
    public function generate(string $idFile, $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        $token = $this->tokenManager->getToken(UploadController::CSRF_INTENTION);

        $url = $this->urlGenerator->generate(
            'file_upload_get_content',
            ['id' => $idFile, 'token' => $token->getValue()],
            $referenceType
        );

        return $url;
    }
}
