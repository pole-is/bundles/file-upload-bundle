<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * {@inheritdoc}
 */
class IrsteaFileUploadExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('irstea_file_upload.max_chunk_size', $config['max_chunk_size']);

        $bundles = $container->getParameter('kernel.bundles');
        if (!isset($bundles['CLTissueBundle'])) {
            // On a pas le CLTissueBundle => pas d'antivirus
            $container->removeDefinition('irstea_file_upload.virus_scanner');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        $pluginDir = __DIR__ . '/../../../../blueimp/jquery-file-upload';

        $container->prependExtensionConfig(
            'assetic',
            [
                'assets' => [
                    'form_js'  => [
                        'inputs' => [
                            "$pluginDir/js/jquery.fileupload.js",
                            "$pluginDir/js/jquery.fileupload-process.js",
                            "$pluginDir/js/jquery.fileupload-validate.js",
                            "$pluginDir/js/jquery.fileupload-ui.js",
                            '@BazingaJsTranslationBundle/Resources/js/translator.js',
                            'js/translations/file_upload/*.js',
                            'js/translations/validators/*.js',
                            '@IrsteaFileUploadBundle/Resources/js/widget/file_upload.js',
                        ],
                    ],
                    'form_css' => [
                        'inputs' => [
                            "$pluginDir/css/jquery.fileupload.css",
                            "$pluginDir/css/jquery.fileupload-ui.css",
                            '@IrsteaFileUploadBundle/Resources/less/file_upload.less',
                        ],
                    ],
                ],
            ]
        );

        $container->prependExtensionConfig(
            'twig',
            [
                'form_themes' => [
                    'IrsteaFileUploadBundle:Form:file_upload.html.twig',
                ],
            ]
        );
    }
}
