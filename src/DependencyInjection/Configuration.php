<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->root('irstea_file_upload');

        $rootNode
            ->children()
            ->integerNode('max_chunk_size')
            ->min(0)
            ->beforeNormalization()
            ->ifString()
            ->then(
                function ($v) {
                    $int = (int) $v;
                    if (strpos($v, 'K')) {
                        return 1000 * $int;
                    }
                    if (strpos($v, 'M')) {
                        return 1000000 * $int;
                    }

                    return $int;
                }
            )
            ->end()
            ->defaultValue(0)
            ->treatNullLike(0)
            ->treatFalseLike(0)
            ->end()
            ->end();

        return $treeBuilder;
    }
}
