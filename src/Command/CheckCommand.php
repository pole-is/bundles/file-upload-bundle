<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Irstea\FileUploadBundle\Model\FileManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of GarbageCollectorCommand.
 */
class CheckCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('irstea:file-upload:check')
            ->setDescription("Vérifie l'intégrité des fichiers.");
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var FileManagerInterface $manager */
        $manager = $this->getContainer()->get('irstea_file_upload.file_manager');

        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $files = $manager->findFilesToValidate();

        if (count($files) == 0) {
            $output->writeln('Aucun fichier à vérifier.');

            return;
        }

        $output->writeln('Vérification des fichiers');

        $progress = new ProgressBar($output, count($files));
        $progress->start();

        foreach ($files as $file) {
            $file->validate();
            if (!$file->isValid()) {
                $output->writeln(sprintf("\nFichier %s: %s", $file->getEtat(), $file->getPath()));
                $em->persist($file);
            }
            $progress->advance();
        }

        $progress->finish();

        $em->flush();
        $output->writeln('');
    }
}
