<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Command;

use Irstea\FileUploadBundle\Model\FileManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of GarbageCollectorCommand.
 */
class ReadCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('irstea:file-upload:read')
            ->setDescription("Récupère le contenu d'un fichier.")
            ->addOption('append', 'a', InputOption::VALUE_NONE, "Ajoute au fichier de destination au lieu de l'écraser.")
            ->addOption('overwrite', 'y', InputOption::VALUE_NONE, "Ecrase le fichier de destination s'il existe.")
            ->addArgument('id', InputArgument::REQUIRED, 'Identifiant du fichier à récupérer.')
            ->addArgument('filepath', InputArgument::OPTIONAL, 'Chemin du fichier dans lequel écrire.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var FileManagerInterface $manager */
        $manager = $this->getContainer()->get('irstea_file_upload.file_manager');

        $id = $input->getArgument('id');
        $file = $manager->get($id);
        if ($file === null) {
            throw new \RuntimeException("File not found: $id");
        }

        $path = $input->getArgument('filepath');

        if (null == $path) {
            $fh = fopen('php://stdout', 'wb');
        } else {
            if ($input->getOption('append')) {
                $fh = fopen($path, 'ab');
            } elseif ($input->getOption('overwrite')) {
                $fh = fopen($path, 'wb');
            } else {
                $fh = fopen($path, 'xb');
            }
        }

        $written = $file->copyTo($fh);
        fclose($fh);

        if ($output->isVerbose() && null !== $path) {
            $output->writeln(sprintf('%d octets écrits.', $written));
        }
    }
}
