<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Listener;

use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Irstea\FileUploadBundle\Model\UploadedFileInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Ajoute les informations sur la date, l'utilisateur connecté et l'adresse IP du client dans le fichier.
 */
class CreationDataListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(TokenStorageInterface $tokenStorage, RequestStack $requestStack)
    {
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
    }

    public function prePersist(UploadedFileInterface $file, LifecycleEventArgs $event)
    {
        $file->setCreatedAt(new DateTime('now'));

        if ($request = $this->requestStack->getCurrentRequest()) {
            $file->setCreatedFrom($request->getClientIp());
        }

        if ($token = $this->tokenStorage->getToken()) {
            $file->setCreatedBy($token->getUsername());
        }
    }
}
