<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Listener;

use Irstea\FileUploadBundle\Event\FileUploadCompleteEvent;
use Irstea\FileUploadBundle\Exception\RejectedFileException;
use Xenolope\Quahog\Client;
use Xenolope\Quahog\Exception\ConnectionException;

/**
 * Description of AntivirusListener.
 */
class VirusScannerListener
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @throws ConnectionException
     */
    public function onFileUploadCompleted(FileUploadCompleteEvent $event)
    {
        $client = $this->getClient();

        $file = $event->getUploadedFile();
        $path = $file->getLocalPath();

        /** @var array $result */
        $result = $client->scanFile($path);

        $status = $result['status'] ?? Client::RESULT_ERROR;
        $hasVirus = $status === Client::RESULT_FOUND;

        $meta = $file->getMetadata();
        $meta['virus_scanner'] = ['has_virus' => $hasVirus];

        if ($hasVirus) {
            $desc = $result['reason'] ?? '???';
            $meta['virus_scanner']['detected'] = $result['reason'];
        } else {
            $desc = false;
        }

        $file->setMetadata($meta);

        if ($hasVirus) {
            throw new RejectedFileException($file, $desc ? sprintf('Found the %s virus !', $desc) : 'Virus found !');
        }
    }

    /**
     * @throws ConnectionException
     */
    private function getClient(): Client
    {
        // Check clamd server's state
        $this->client->ping();

        return $this->client;
    }
}
