<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Gaufrette\Exception\FileNotFound;
use Gaufrette\Filesystem;
use Irstea\FileUploadBundle\Model\UploadedFileInterface;

/**
 * Listener qui traite les opérations sur les fichiers uploadés.
 */
class UploadedFileListener
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var array
     */
    private $scheduledDeletion = [];

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /** Passe le filesystem à l'entité.
     */
    public function postLoad(UploadedFileInterface $file, LifecycleEventArgs $event)
    {
        $file->setFilesystem($this->filesystem);
    }

    /** Renomme le fichier disque lorsque l'on change l'attribut 'path'.
     */
    public function postUpdate(UploadedFileInterface $file, LifecycleEventArgs $event)
    {
        $changes = $event->getEntityManager()->getUnitOfWork()->getEntityChangeSet($file);

        if (!isset($changes['path'])) {
            return;
        }

        $this->filesystem->rename($changes['path'][0], $changes['path'][1]);
    }

    /** Enregistre le chemin du fichier à supprimer.
     */
    public function preRemove(UploadedFileInterface $file, LifecycleEventArgs $event)
    {
        $this->scheduledDeletion[$file->getId()] = $file->getPath();
    }

    /** Supprime le fichier correspondant à l'UploadedFileInterface supprimé.
     */
    public function postRemove(UploadedFileInterface $file, LifecycleEventArgs $event)
    {
        try {
            $this->filesystem->delete($this->scheduledDeletion[$file->getId()]);
        } catch (FileNotFound $ex) {
            // NOOP
        }
    }

    /**
     * Nettoie la liste des fichiers à supprimer.
     */
    public function postFlush()
    {
        $this->scheduledDeletion = [];
    }
}
