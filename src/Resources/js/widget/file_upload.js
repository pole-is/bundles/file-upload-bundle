/*
 * Copyright (C) 2015 IRSTEA
 * All rights reserved.
 */
(function ($, Translator) {

    var formatFileSize = function (size) {
            var unit;
            if (size > 1000000000) {
                size = (size / 1000000000).toFixed(2);
                unit = 'Gi';
            } else if (size > 1000000) {
                size = (size / 1000000).toFixed(2);
                unit = 'Mi';
            } else if (size > 1000) {
                size = (size / 1000).toFixed(2);
                unit = 'Ki';
            } else {
                unit = '';
            }

            return Translator.trans('file_size(%size%,%unit%)', {size: size, unit: unit}, 'file_upload');
        },
        formatBitrate = function (rate) {
            return formatFileSize(rate) + '/s';
        };

    $.blueimp.fileupload.prototype._formatFileSize = formatFileSize;
    $.blueimp.fileupload.prototype._formatBitrate = formatBitrate;

    /** Plugin irsteaFileUpload.
     */
    $.fn.irsteaFileUpload = function (options) {

        var $this = $(this),
            $button = $this.find('.fileinput-button'),
            input = $this.find(':file')[0],
            $entries = $this.find('.fileinput-entries'),
            nextIndex = $entries.find('.fileinput-entry').length,
            createUrl = options.createUrl,
            uploadPrototype = options.uploadPrototype,
            downloadPrototype = options.downloadPrototype,
            csrfQuery = '?token=' + options.csrfToken;

        delete options.createUrl;
        delete options.uploadPrototype;
        delete options.downloadPrototype;
        delete options.csrfToken;

        if (options.acceptFileTypes) {
            options.acceptFileTypes = new RegExp('^' + options.acceptFileTypes + '$');
        }

        var checkValidity = function () {
                var counts = {
                        upload: $entries.find('.template-upload').length,
                        download: $entries.find('.template-download').length,
                        error: $entries.find('.alert-danger').length,
                    },
                    error = '';

                if (counts.upload > 0) {
                    error = Translator.trans('file_upload.runningUpload', null, 'validators');

                } else if (counts.error > 0) {
                    error = Translator.trans('file_upload.uploadError', null, 'validators');

                } else if (options.required && counts.download < 1) {
                    error = Translator.trans('file_upload.required', null, 'validators');
                }

                input.setCustomValidity(error);

                // console.debug(input, counts, input.validationMessage, input.validity);
            },
            showError = function (entry, message) {
                var $this = $(entry);
                if (message) {
                    $this.find('.error').text(Translator.trans(message));
                }
                $this.addClass('alert alert-danger');
                $this.find('.error').show();
                $this.find('.icon')
                    .removeClass('fa-circle-o-notch fa-spin fa-file-o')
                    .addClass('fa-exclamation-triangle');
                $this.find('.description').remove();
                $this.find('.id').remove();
            },
            updateDisplay = function (event) {
                var hasEntry = false;
                $entries.find('.fileinput-entry').each(function () {
                    var data = $(this).data('data');
                    if (data && data.files.error) {
                        showError(this, data.files[0].error);
                    }
                    hasEntry = true;
                });
                $button.toggle(options.multiple || !hasEntry);
                checkValidity();
            };

        // Activation
        $button.fileupload($.extend(
            options,
            {
                type: 'PUT',
                autoUpload: true,
                formData: {},
                multipart: false,
                uploadTemplateId: null,
                downloadTemplateId: null,
                filesContainer: $this.find('.fileinput-entries'),
                dropZone: $this,
                i18n: function(key, values, domain) {
                    var res = Translator.trans(key, values, domain || 'file_upload');
                    if (res !== key) {
                        return res;
                    }
                    res = Translator.trans('file_upload.'+ key, values, 'validators');
                    if (res !== 'file_upload.'+ key) {
                        return res;
                    }
                    return Translator.trans(key, values, 'messages');
                },
                uploadTemplate: function (data) {
                    var rows = $();
                    $.each(data.files, function (index, file) {
                        var row = $(uploadPrototype);
                        rows = rows.add(row);
                        row.find('.name').text(file.name);
                        if (file.error) {
                            showError(row, file.error);
                            return;
                        }
                        row.find('.size').text(formatFileSize(file.size));
                    });
                    return rows;
                },
                downloadTemplate: function (data) {
                    var rows = $();
                    $.each(data.files, function (index, file) {
                        var row = $(downloadPrototype.replace(/__index__/g, nextIndex++));
                        rows = rows.add(row);
                        if (file.error) {
                            row.find('.name').text(file.name);
                            showError(row, file.error);
                            return;
                        }
                        row.find('.repr').html(file.repr);
                        row.find('.delete')
                            .attr('data-type', file.delete_type)
                            .attr('data-url', file.delete_url + csrfQuery);
                        row.find('input.id')
                            .val(file.id);
                    });
                    return rows;
                },
                submit: function (e, data) {
                    var $this = $(this),
                        file = data.files[0];
                    $.post(
                        createUrl,
                        {file: {name: file.name, size: file.size, type: file.type, lastModified: file.lastModified}},
                        function (response) {
                            file.icon = response.icon;
                            data.url = response.put_url + csrfQuery;
                            data.delete_url = response.delete_url;
                            data.delete_type = response.delete_type;
                            data.jqXHR = $this.fileupload('send', data);
                        }
                    )
                        .fail(function (jqXHR, textStatus, errorThrown) {
                            file.error = textStatus === "error" ? errorThrown : ('Error #' + jqXHR.status);
                            data.files.error = true;
                            showError(data.context, file.error);
                        })
                        .always(updateDisplay);
                    return false;
                },
                progress: function (e, data) {
                    if (!data.context || e.isDefaultPrevented()) {
                        return;
                    }
                    var percent = data.loaded / data.total * 100,
                        percentText = percent.toFixed(1);
                    data.context.each(function () {
                        var $this = $(data.context);
                        $this.find('.progress').show();
                        $this.find('.progress-bar').css('width', percent + '%').attr('aria-valuenow', percentText);
                        $this.find('.progress-text').show().html(percentText + '% (' + formatBitrate(data.bitrate) + ')');
                    });
                },
                getFilesFromResponse: function (data) {
                    var files = [];
                    $.each(data.files, function (index, file) {
                        files.push($.extend(file, data.result.files[index]));
                    });
                    return files;
                },
            }
        )).bind({
            fileuploadfailed: function (event, data) {
                if (data.delete_url) {
                    $.ajax(data.delete_url + csrfQuery, {type: data.delete_type});
                    checkValidity();
                }
            },
            fileuploadadded: updateDisplay,
            fileuploadfinished: updateDisplay,
            fileuploaddestroyed: updateDisplay,
            fileuploadprocessalways: updateDisplay
        });

        if (options.disabled || options.readonly) {
            $button.fileupload('disable');
        }

        updateDisplay();
    };

})(jQuery, Translator);
