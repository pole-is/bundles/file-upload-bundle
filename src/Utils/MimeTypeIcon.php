<?php declare(strict_types=1);
/*
 * irstea/file-upload-bundle - Bundle de gestion de fichiers intégrée à Symfony et Twitter-Bootstrap.
 * Copyright (C) 2015-2019 Irstea <dsi.poleis.contact@lists.irstea.fr>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\FileUploadBundle\Utils;

/**
 * Description of MimeTypeIcon.
 */
final class MimeTypeIcon
{
    /**
     * @var array
     */
    private static $icons = [
        'application/vnd.oasis.opendocument.text'                                   => 'word',
        'application/vnd.oasis.opendocument.spreadsheet'                            => 'excel',
        'application/vnd.oasis.opendocument.presentation'                           => 'powerpoint',
        'application/vnd.oasis.opendocument.graphics'                               => 'image',
        'application/vnd.ms-excel'                                                  => 'excel',
        'application/vnd.ms-powerpoint'                                             => 'powerpoint',
        'application/msword'                                                        => 'word',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'         => 'excel',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation' => 'powerpoint',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document'   => 'word',
        'application/pdf'                                                           => 'pdf',
        'application/zip'                                                           => 'archive',
        'application/x-rar-compressed'                                              => 'archive',
        'application/x-gtar'                                                        => 'archive',
        'application/x-gzip'                                                        => 'archive',
        'application/x-tar'                                                         => 'archive',
    ];

    /**
     * @param string $mimeType
     */
    public static function getMimeTypeIcon($mimeType): ?string
    {
        if (!is_string($mimeType)) {
            return null;
        }

        $sep = strpos($mimeType, ';');
        if ($sep !== false) {
            $mimeType = trim(substr($mimeType, 0, $sep));
        }

        if (isset(self::$icons[$mimeType])) {
            return self::$icons[$mimeType];
        }

        $matches = [];
        if (preg_match('@^(text|audio|video|image)/@', $mimeType, $matches)) {
            return $matches[1];
        }

        return null;
    }
}
